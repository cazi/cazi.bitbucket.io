let prints = document.getElementById('prints');

function randInt(max) {
	return Math.floor(Math.random() * max);
}

function colourChange(event) {
	changeColour(event.target.value);
}

function randomColour(_event) {
	let r = randInt(255).toString(16);
	let g = randInt(255).toString(16);
	let b = randInt(255).toString(16);

	resultBox = document.getElementById("random-result")
	resultBox.innerHTML = `${parseInt("0x" + r[0] + r[1])}, ${parseInt("0x" + g[0] + g[1])}, ${parseInt("0x" + b[0] + b[1])}`
	changeColour("#" + r + g + b);
}

function changeColour(colour) {
	// this function expects a colour in the hex #ffffff format

	prints.style.backgroundColor = colour;

	let r = parseInt("0x" + colour[1] + colour[2])
	let g = parseInt("0x" + colour[3] + colour[4])
	let b = parseInt("0x" + colour[5] + colour[6])
	let average = (r + g + b) / 3
	
	if (average >= 255 * 0.30) { // if more than 30%
		prints.style.color = "#000";
	} else {
		prints.style.color = "#fff";
	}
}

window.onload = function(){
	for (var i = 1; i <= 822; i++) {
		let div = document.createElement("div")
		div.className = "print"
		let image = document.createElement("img")
		image.src = `img/${i}.png`
		image.alt = `Print ${i}`
		let text = document.createElement("p")
		text.innerHTML = `Print ${i}`

		div.appendChild(text)
		div.appendChild(image)
		prints.appendChild(div)
	}

	document.getElementById("backgroundPicker").addEventListener("input", colourChange);
	document.getElementById("randomColourBtn").addEventListener("click", randomColour);
}


